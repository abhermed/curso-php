<html>
<!-- Abraham Hernández Medina-->
    <head>
        <title>tarea3_AHM</title>
    </head>
    <h3><strong>Expresiones regulares - Abraham Hdz</strong></h3>
    
    <body>  
    <p><strong>Expresión regular para emails </strong></p>
    <?php
    //Realizar una expresión regular que detecte emails correctos. 
    $str="correo@mail.com";
    $result = preg_match("/^[a-zA-Z0-9\._-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/", $str);
    echo $str;
    echo "</br>";
    echo $result, ": Correcto.";    
    ?>

    <p><strong>Expresión regular para CURPS </strong></p>
    <?php
    //Realizar una expresion regular que detecte Curps Correctos 
    //ABCD123456EFGHIJ78. 
    $str = 'XOVRE7686352';
    $result = preg_match("/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/", $str);
    echo $str;
    echo "</br>";
    echo $result, ": Incorrecto";
    ?>

    <p><strong>Expresión regular para palabras de longitud mayor a 50</strong></p>
    <?php
    //Realizar una expresion regular que detecte palabras de longitud mayor a 50 
    //formadas solo por letras. 
    $str = 'UnaCadenaQueDeberiaSerValidaDeAcuerdoAFuncionRegexParaLongitudMasCincuenta';
    $result = preg_match("/^[a-zA-Z]{50,}$/", $str);
    echo $str;
    echo "</br>";
    echo $result, ": Correcta";  
    ?>

    <p><strong>Funcion para escapar simbolos especiales </strong></p>
    <?php
    //Crea una funcion para escapar los simbolos especiales. 
    $str = "&Prueba%";
    $result = preg_replace('([^A-Za-z0-9])', ' ', $str);
    echo $str;
    echo "</br>";
    echo $result;
    ?>

    <p><strong>Expresión regular para números decimales </strong></p>
    <?php
    //Crear una expresion regular para detectar números decimales. 
    $str = '2.10';
    $result = preg_match("/^\d*\.?\d*$/", $str);
    echo $str; 
    echo "</br>";
    echo $result, ": Decimal detectado";  
    ?>

    </body>

    <style>
    body {
    	margin: 10px auto;
    	text-align:left;
    	font-size: 20px;
    	font-family: "Arial";
        }
     
     p {
    	font-weight: 70;
    	font-family: 'Arial';
    	text-align:left;
        }
 
    </style>
</html>
